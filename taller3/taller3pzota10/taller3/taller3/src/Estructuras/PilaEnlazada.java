package Estructuras;
import java.util.Iterator;
import java.util.NoSuchElementException;
public class PilaEnlazada <T> implements Iterable<T>
{
	


	/**
	 *  The <tt>LinkedStack</tt> class represents a last-in-first-out (LIFO) stack of
	 *  generic items.
	 *  It supports the usual <em>push</em> and <em>pop</em> operations, along with methods
	 *  for peeking at the top item, testing if the stack is empty, and iterating through
	 *  the items in LIFO order.
	 *  <p>
	 *  This implementation uses a singly-linked list with a non-static nested class for 
	 *  linked-list NodoPilas. See {@link Stack} for a version that uses a static nested class.
	 *  The <em>push</em>, <em>pop</em>, <em>peek</em>, <em>size</em>, and <em>is-empty</em>
	 *  operations all take constant time in the worst case.
	 *  <p>
	 *  For additional documentation,
	 *  see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
	 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
	 *
	 *  @author Robert Sedgewick
	 *  @author Kevin Wayne
	 */
 
	    private int n;          // size of the stack
	    private NodoPila<T> first;     // top of stack
	    private int maxSize;
	    
	    /**
	     * Initializes an empty stack.
	     */
	    public PilaEnlazada(int pMax) {
	        first = null;
	        n = 0;
	        maxSize = pMax;
	        assert check();
	    }

	    /**
	     * Is this stack empty?
	     * @return true if this stack is empty; false otherwise
	     */
	    public boolean isEmpty() {
	        return first == null;
	    }

	    /**
	     * Returns the number of items in the stack.
	     * @return the number of items in the stack
	     */
	    public int size() {
	        return n;
	    }
	   
	    /**
	    * Retorna el primer nodo de la pila. <br>
	    * <b>post: </b> Se retorn� el primer nodo de la pila.
	    * @return El primer nodo de la pila
	    */
	    public NodoPila<T> darPrimero( )
	    {
	    return first;
	    }

	    /**
	     * Adds the item to this stack.
	     * @param item the item to add
	     */
	    public boolean push(T item) {
	    	
	    	if(n<maxSize)
	    	{
	    		if(first==null)
	    		{
	    			first = new NodoPila<T>(item);
	    			n++;
	    			return true;
	    		}
	    		else
	    		{
	    		NodoPila<T> nodo = new NodoPila<T>( item );
	    		 NodoPila<T> oldfirst = first;
	  	        first = nodo;
	  	        first.insertarDespues(oldfirst);
	  	        n++;
	  	        assert check();
	  	        return true;
	    		}
	    	}
	    	else
	    	{
	    		return false;
	    	}
	      
	    }

	    /**
	     * Removes and returns the item most recently added to this stack.
	     * @return the item most recently added
	     * @throws java.util.NoSuchElementException if this stack is empty
	     */
	    public T Pop() 
	    {
	        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
	        T item = first.darElemento();        // save item to return
	        first = first.darSiguiente();            // delete first NodoPila
	        n--;
	        assert check();
	        return item;                   // return the saved item
	    }


	    /**
	     * Returns (but does not remove) the item most recently added to this stack.
	     * @return the item most recently added to this stack
	     * @throws java.util.NoSuchElementException if this stack is empty
	     */
	    public T peek() {
	        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
	        return first.darElemento();
	    }

	    /**
	     * Returns a string representation of this stack.
	     * @return the sequence of items in the stack in LIFO order, separated by spaces
	     */
	    public String toString() 
	    {
	        StringBuilder s = new StringBuilder();
	        for (T item : this)
	            s.append(item + " ");
	        return s.toString();
	    }
	       
	    
	    /**
	     * Returns an iterator to this stack that iterates through the items in LIFO order.
	     * @return an iterator to this stack that iterates through the items in LIFO order.
	     */
	    public Iterator<T> iterator() 
	    {
	        return new ListIterator();
	    }

	    // an iterator, doesn't implement remove() since it's optional
	    private class ListIterator implements Iterator<T> {
	        private NodoPila actual = first;
	        public boolean hasNext()  
	        { 
	        	return actual != null;                    
	        }
	        
	        public void remove()      
	        { throw new UnsupportedOperationException();  
	        }

	        public T next() 
	        {
	            if (!hasNext()) throw new NoSuchElementException();
	            T item = (T) actual.darElemento();
	            actual = actual.darSiguiente(); 
	            return item;
	        }
	    }


	    // check internal invariants
	    private boolean check() {

	        // check a few properties of instance variable 'first'
	        if (n < 0) 
	        {
	            return false;
	        }
	        if (n == 0) 
	        {
	            if (first != null) return false;
	        }
	        else if (n == 1) {
	            if (first == null)      return false;
	            if (first.darSiguiente() != null) return false;
	        }
	        else {
	            if (first == null)      return false;
	            if (first.darSiguiente() == null) return false;
	        }

	        // check internal consistency of instance variable n
	        int numberOfNodoPilas = 0;
	        for (NodoPila x = first; x != null && numberOfNodoPilas <= n; x = x.darSiguiente()) {
	            numberOfNodoPilas++;
	        }
	        if (numberOfNodoPilas != n) return false;

	        return true;
	    }

	    /**
	     * Unit tests the <tt>LinkedStack</tt> data type.
	     */
	    public static void main(String[] args) {
//	        while (!StdIn.isEmpty()) {
//	            String item = StdIn.readString();
//	            if (!item.equals("-"))
//	                stack.push(item);
//	            else if (!stack.isEmpty())
//	                StdOut.print(stack.pop() + " ");
//	        }
//	        StdOut.println("(" + stack.size() + " left on stack)");
	    }
	}



