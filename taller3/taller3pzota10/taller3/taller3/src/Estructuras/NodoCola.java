package Estructuras;

public class NodoCola <T> {
	
	private T elem;
	
	/**
	* Siguiente elemento encadenado
	*/
	private NodoCola<T> sigNodo;
	
	
	/**
	* Constructor por par�metros del nodo. <br>
	* <b>post: </b> Se construy� el nodo con el elemento especificado, sigNodo=null y elemento = pElemento.<br>
	* @param pElemento Elemento que va a ser almacenado en el nodo. Diferente de null<br>
	*/
	public NodoCola( T pElemento )
	{
	elem = pElemento;
	sigNodo = null;
	}
	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	* Retorna el elemento del nodo. <br>
	* <b>post: </b> Se retorn� el elemento contenido en el nodo.<br>
	* @return El elemento contenido en el nodo. Diferente de null<br>
	*/
	public T darElemento( )
	{
	return elem;
	}

/**
* Desconecta el nodo de la cola suponiendo que es el primero. <br>
* <b>pre: </b> El nodo actual es el primer nodo de la cola. <br>
* <b>post: </b> Se retorn� el nodo con el cual comienza la cola ahora, sigNodo=null.<br>
* @return Nodo con el cual comienza la cola ahora.<br>
*/
public NodoCola<T> desconectarPrimero( )
{
NodoCola<T> p = sigNodo;
sigNodo = null;
return p;
}



/**
* Inserta el nodo especificado despu�s del nodo actual. <br>
* <b>post: </b> Se insert� el nodo despu�s del nodo actual lo que implica que sigNodo=nodo.<br>
* @param nodo El nodo a ser insertado<br>
* @return Nodo que se insert� despu�s del nodo actual<br>
*/
public NodoCola<T> insertarDespues( NodoCola<T> nodo )
{
sigNodo = nodo;
return nodo;
}
/**
* Retorna el siguiente nodo. <br>
* <b>post: </b> Se retorn� el siguiente nodo.<br>
* @return El nodo siguiente<br>
*/
public NodoCola<T> darSiguiente( )
{
return sigNodo;
}
}
