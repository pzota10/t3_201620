package Estructuras;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *  The <tt>LinkedQueue</tt> class represents a first-in-first-out (FIFO)
 *  queue of generic items.
 *  It supports the usual <em>enqueue</em> and <em>dequeue</em>
 *  operations, along with methods for peeking at the first item,
 *  testing if the queue is empty, and iterating through
 *  the items in FIFO order.
 *  <p>
 *  This implementation uses a singly-linked list with a non-static nested class 
 *  for linked-list nodes.  See {@link Queue} for a version that uses a static nested class.
 *  The <em>enqueue</em>, <em>dequeue</em>, <em>peek</em>, <em>size</em>, and <em>is-empty</em>
 *  operations all take constant time in the worst case.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/13stacks">Section 1.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class ColaEnlazada<T> implements Iterable<T> {
    private int n;         // number of elements on queue
    private Node first;    // beginning of queue
    private Node last;     // end of queue
    

    // helper linked list class
    private class Node {
        private T item;
        private Node next;
    }

    /**
     * Initializes an empty queue.
     */
    public ColaEnlazada()  {
        first = null;
        last  = null;
        n = 0;
        assert check();
    }

    /**
     * Is this queue empty?
     * @return true if this queue is empty; false otherwise
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Returns the number of items in this queue.
     * @return the number of items in this queue
     */
    public int size() {
        return n;     
    }

    /**
     * Returns the item least recently added to this queue.
     * @return the item least recently added to this queue
     * @throws java.util.NoSuchElementException if this queue is empty
     */
    public T peek() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        return first.item;
    }

    /**
     * Adds the item to this queue.
     * @param item the item to add
     */
    public void enqueue(T item) 
    {
        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if (isEmpty()) first = last;
        else           oldlast.next = last;
        n++;
        assert check();
    }

    /**
     * Removes and returns the item on this queue that was least recently added.
     * @return the item on this queue that was least recently added
     * @throws java.util.NoSuchElementException if this queue is empty
     */
    public T dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        T item = first.item;
        first = first.next;
        n--;
        if (isEmpty()) last = null;   // to avoid loitering
        assert check();
        return item;
    }

    /**
     * Returns a string representation of this queue.
     * @return the sequence of items in FIFO order, separated by spaces
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (T item : this)
            s.append(item + " ");
        return s.toString();
    } 

    // check internal invariants
    private boolean check() {
        if (n < 0) {
            return false;
        }
        else if (n == 0) {
            if (first != null) return false;
            if (last  != null) return false;
        }
        else if (n == 1) {
            if (first == null || last == null) return false;
            if (first != last)                 return false;
            if (first.next != null)            return false;
        }
        else {
            if (first == null || last == null) return false;
            if (first == last)      return false;
            if (first.next == null) return false;
            if (last.next  != null) return false;

            // check internal consistency of instance variable n
            int numberOfNodes = 0;
            for (Node x = first; x != null && numberOfNodes <= n; x = x.next) {
                numberOfNodes++;
            }
            if (numberOfNodes != n) return false;

            // check internal consistency of instance variable last
            Node lastNode = first;
            while (lastNode.next != null) {
                lastNode = lastNode.next;
            }
            if (last != lastNode) return false;
        }

        return true;
    } 
 

    /**
     * Returns an iterator that iterates over the items in this queue in FIFO order.
     * @return an iterator that iterates over the items in this queue in FIFO order
     */
    public Iterator<T> iterator()  {
        return new ListIterator();  
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator implements Iterator<T> {
        private Node actual = first;

        public boolean hasNext()  
        { return actual != null;                     
        }
        public void remove()      
        { throw new UnsupportedOperationException();  
        }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T item = actual.item;
            actual = actual.next; 
            return item;
        }
    }


    /**
     * Unit tests the <tt>LinkedQueue</tt> data type.
     */
    public static void main(String[] args) {

    }
}
//public class Cola <T>
//{
//
//	protected NodoCola primero;
//	protected NodoCola ultimo;
//	
//	protected int numElems;
//
//// -----------------------------------------------------------------
//// Constructores
//// -----------------------------------------------------------------
//
///**
// * Constructor de la cola encadenada vac�a. 
//
// * post:  Se construy� una cola vac�a. primero==null, ultimo==null, numElems = 0
//
// */
//public Cola( )
//{
//    primero = null;
//    ultimo = null;
//    numElems = 0;
//}
//
//// -----------------------------------------------------------------
//// M�todos
//// -----------------------------------------------------------------
///**
// * Retorna la longitud de la cola (n�mero de elementos). 
//
// * post:  Se retorn� la longitud de la cola
//.
// * @return El n�mero de elementos de la cola. Entero positivo o cero.
//
// */
//public int darLongitud( )
//{
//    return numElems;
//}
///**
// * Inserta un elemento al final de la cola. 
//
// * post:  Se agreg� el elemento especificado al final de la cola. Si la cola es vac�a, el primer y el ultimo elemento son iguales
//
// * @param elemento El elemento a ser insertado. Diferente de null.
//
// */
//
//
//
//public void insertar( T elemento )
//{
//    NodoCola<T> nodo = new NodoCola<T>( elemento );
//    if( primero == null )
//    {
//        primero = nodo;
//        ultimo = nodo;
//    }
//    else
//    {
//        ultimo = ultimo.insertarDespues(nodo);
//    }
//    numElems++;
//}
//


