package parqueadero;

import java.util.Date;
import java.util.Iterator;





import java.util.NoSuchElementException;

import Estructuras.ColaEnlazada;
import Estructuras.NodoPila;
import Estructuras.PilaEnlazada;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	
	private ColaEnlazada<Carro> carroEspera;
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */

    private PilaEnlazada<Carro>[] parqueaderos;
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
   
    private PilaEnlazada<Carro> temporal;
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
       temporal = new PilaEnlazada<Carro>(4);
        parqueaderos= new PilaEnlazada[8];
        for (int i = 0; i < 8; i++) {
			parqueaderos[i] = new PilaEnlazada<Carro>(8);
		}
        carroEspera = new ColaEnlazada<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
   Carro nuevo= new Carro (	pColor,pMatricula,pNombreConductor);
    	carroEspera.enqueue(nuevo);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	Carro sigEnCola = carroEspera.dequeue();
    	for (int i = 0; i < parqueaderos.length; i++) {
			if(parqueaderos[i].size()<4)
			{
				parqueaderos[i].push(sigEnCola);
				break;
			}
		}
    	
      return sigEnCola.darMatricula();
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro sale = sacarCarro(matricula);
    	if(sale==null)
    	{throw new NoSuchElementException("no hay carro con placa");
    	}
    
    	return cobrarTarifa(sale);
    }
    
  /**
   * Busca un parqueadero con cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    { String n= "";
    	for (int i=0; i<parqueaderos.length;i++)
    	{
    		if(parqueaderos[i].size()<4)
    		{
    			parqueaderos[i].push(aParquear);
    			n =Integer.toString(i);
    					
    		}
    		else throw new Exception("ningun parqueadero con cupo");
    	}
    	return "PARQUEADERO"+n;    	
    }
    		    
    
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro(String matricula)
    {
    	Carro c= null;
    	for (int i = 0; i<parqueaderos.length&& c==null;i++)
    	{
    		if(i==0)
    		{
    			c= sacarCarroP1(matricula);
    		}
    	else if(i==1)
		{
			c= sacarCarroP2(matricula);
		}
    	else if(i==2)
		{
			c= sacarCarroP3(matricula);
		}
    	else if(i==3)
		{
			c= sacarCarroP4(matricula);
		}
    	else if(i==4)
		{
			c= sacarCarroP5(matricula);
		}
    	else if(i==6)
		{
			c= sacarCarroP7(matricula);
		}
    	else 
		{
			c= sacarCarroP8(matricula);
		}
    	}
    	return c;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par1 = parqueaderos[0];
    	for(int i=0; i<par1.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par1.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par1.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par1.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par1.Pop());
    		}	
    	}
    	return c;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par2 = parqueaderos[1];
    	for(int i=0; i<par2.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par2.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par2.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par2.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par2.Pop());
    		}	
    	}
    	return c;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par3 = parqueaderos[2];
    	for(int i=0; i<par3.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par3.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par3.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par3.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par3.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par4 = parqueaderos[3];
    	for(int i=0; i<par4.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par4.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par4.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par4.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par4.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par1 = parqueaderos[0];
    	for(int i=0; i<par1.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par1.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par1.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par1.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par1.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par1 = parqueaderos[0];
    	for(int i=0; i<par1.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par1.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par1.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par1.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par1.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par1 = parqueaderos[0];
    	for(int i=0; i<par1.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par1.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par1.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par1.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par1.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	Carro c = null;
    	int contador=0;
    	PilaEnlazada<Carro> par1 = parqueaderos[0];
    	for(int i=0; i<par1.size()&&c==null;i++)
    	{
    		contador++;
    		Carro actual  = par1.darPrimero().darElemento();
    		if(actual.darMatricula().equals(matricula))
    				{
    			par1.Pop(); 
    			c= actual;
    			
    			if(contador>1)
    				for(int j=contador-1; j>0;j--)
    				{
    					par1.push(temporal.Pop());
    				}
    			return c;
    				}
    		else{
    			
    			temporal.push(par1.Pop());
    		}	
    	}
    	return c;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa(Carro car)
    {
    	long actual = new Date().getTime();
    	long deltaMili = actual-car.darLlegada();
    	
    	double tari = (deltaMili/60000) *25; 
    	return tari;	
    }

	/**
	 * @return the parqueaderos
	 */
	public PilaEnlazada<Carro>[] getParqueaderos() {
		return parqueaderos;
	}

	/**
	 * @param parqueaderos the parqueaderos to set
	 */
	public void setParqueaderos(PilaEnlazada<Carro>[] parqueaderos) {
		this.parqueaderos = parqueaderos;
	}

	/**
	 * @return the carroEspera
	 */
	public ColaEnlazada<Carro> getCarroEspera() {
		return carroEspera;
	}

	/**
	 * @param carroEspera the carroEspera to set
	 */
	public void setCarroEspera(ColaEnlazada<Carro> carroEspera) {
		this.carroEspera = carroEspera;
	}

	/**
	 * @return the temporal
	 */
	public PilaEnlazada<Carro> getTemporal() {
		return temporal;
	}

	/**
	 * @param temporal the temporal to set
	 */
	public void setTemporal(PilaEnlazada<Carro> temporal) {
		this.temporal = temporal;
	}
    
}
